#!/usr/bin/python

import pyparsing as pyp
import itertools
import math
from contextlib import nested

integer = pyp.Word(pyp.nums)

class Parser(object):
    def __init__(self):
        date = "|" + integer + ":" + integer + ":" + integer + "." + integer
        self.__pattern = date + pyp.Regex(".*")

    def parse(self, line):
        parsed = self.__pattern.parseString(line)

        return parsed

parser = Parser()
    
def _time(line):
    try:
        fields = parser.parse(line)
    except pyp.ParseException:
        return (False,0)

    day = 0
    hour = int(fields[1])
    minute = int(fields[3])
    second = int(fields[5])
    micsec = int(fields[7])
    
    return (True, (day, hour, minute, second, micsec))
                                    

def get_metric(line, name):
    pos = line.find(name)
    number = -1
    if pos>=0:
        numberIndex = line.index(" = ", pos)
        number = int(line[numberIndex+3:len(line)-1])

    return number

def main():

    lastsec = 0
    rr_rate = 0
    ad_rate = 0
    rm_rate = 0
    al_rate = 0
    total=0
    cec=0
    cc=0

    # results_rc restore_call_req
    # results_adc conference_add_call_req
    # results_rcf conference_remove_call_req rate
    # results_cip  calls in progress

    with nested(open('./smc_svc.log'), open('results.csv', 'w')) as (syslogFile, csv_file):
        for line in syslogFile:
        
            if not (("restore_call_req" in line) \
                    or ("|  callCount" in line) \
                    or ("callEstablishedCount" in line)\
                    or ("allocate_session_req" in line)\
                    or ("conference_add_call_req" in line)\
		    or ("conference_remove_call_req" in line)): continue

            (Success, Time) = _time(line)

            if Success == False:
                continue

            (day, hour, minute, second, micsec) = Time

            newsec = False
            if lastsec != second:
                lastsec = second

                cip = cc - cec
                total = total + rr_rate

                csv_file.write("%02d:%02d:%02d" % (hour, minute, lastsec))
                csv_file.write("; %d" % (cip))
                csv_file.write("; %d" % (rr_rate))
                csv_file.write("; %d" % (ad_rate))
                csv_file.write("; %d" % (rm_rate))
                csv_file.write("; %d" % (al_rate))
                csv_file.write("\n")

                print "%02d:%02d:%02d Calls:%d Established:%d Progress %d" % (hour, minute, second, cc, cec, cip)
                print "%02d:%02d:%02d restore_call_req rate:           %d" % (hour, minute, lastsec, rr_rate)
                print "%02d:%02d:%02d conference_add_call_req rate:    %d"  % (hour, minute, lastsec, ad_rate)
                print "%02d:%02d:%02d conference_remove_call_req rate: %d"  % (hour, minute, lastsec, rm_rate)
                print "%02d:%02d:%02d allocate_session_req rate: %d"  % (hour, minute, lastsec, al_rate)
                print "\n"

                al_rate = 0
                ad_rate = 0
                rr_rate = 0
                rm_rate = 0
                cip = 0
                cc = 0
                cec =0


            if "restore_call_req" in line:
                rr_rate = rr_rate + 1

            if "conference_add_call_req" in line:
                ad_rate = ad_rate + 1

            if "conference_remove_call_req" in line:
                rm_rate = rm_rate + 1

            if "allocate_session_req" in line:
#                print "%02d:%02d:%02d allocate_session_req:%d" % (hour, minute, second, al_rate)
                al_rate = al_rate + 1

            if "|  callCount" in line:
                cc = get_metric(line, "|  callCount")
#                print "%02d:%02d:%02d Calls:%d" % (hour, minute, second, cc)

            if "|  callEstablishedCount" in line:
                cec = get_metric(line, "|  callEstablishedCount")
 #               print "%02d:%02d:%02d Established:%d" % (hour, minute, second, cec)




    print "total: %d" % (total)

if __name__ == "__main__":
    main()




    
