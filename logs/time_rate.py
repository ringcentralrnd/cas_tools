#!/usr/bin/python

# 2015-06-05T03:18:55.029540+00:00 rms01-t01-rcs03 INFO smc_svc[3355]: |rSip#12.3474   

import pyparsing as pyp
import itertools
import math

integer = pyp.Word(pyp.nums)

# day hour min sec
start_test = (9,19,20,00)
stop_test = (9,20,50,00)


class Parser(object):
    def __init__(self):
        date = integer + "-" + integer + "-" + integer + "T" + integer + ":" + integer + ":" + integer + "." + integer
# CAS        date = integer + "-" + integer + "-" + integer + "T" + integer + ":" + integer + ":" + integer
        self.__pattern = date + pyp.Regex(".*")

    def parse(self, line):
        parsed = self.__pattern.parseString(line)

        return parsed

""" --------------------------------- """

def main():
    parser = Parser()

    start = start_test[1]*3600 + start_test[2]*60 +start_test[3]
    stop = stop_test[1]*3600 + stop_test[2]*60 +stop_test[3]

    with open('rcs-smc_svc.log') as syslogFile:
        lastsecond = 0
        lastminute = 0
        lasthour = 0
        lastday = 0
        burstmicsec = 0
        burstsec = 0
        burstmin = 0
        bursthour = 0
        burstlines = 0
        burstbytes = 0
        for line in syslogFile:

            burstlines += 1
            burstbytes += len(line)
            
            try:
                fields = parser.parse(line)
            except pyp.ParseException:
 #               print line
                continue

            day = int(fields[4])
            hour = int(fields[6])
            minute = int(fields[8])
            second = int(fields[10])
            micsec = int(fields[12])

            current = (second+minute*60+hour*3600)*1000000+micsec
            diff = abs(current/1000000-(lastsecond+lastminute*60+lasthour*3600))
            burst = (burstsec+burstmin*60+bursthour*3600)*1000000+burstmicsec
            burstdiff = abs(current-burst)
            if lastminute != minute:
                print "%02d:%02d:%02d.%d" % (hour, minute, second, micsec)


            lastsecond = second
            lastminute = minute
            lasthour = hour
            lastday = day

#            print "%d %02d:%02d:%02d" % (burstdiff, hour, minute, second)

            if burstdiff >= 5000000:
                bursthour = hour
                burstsec = second
                burstmin = minute
                burstmicsec = micsec
                print "burst (%.2f sec) %d lines, %.2f MBytes/s" % (burstdiff/1000000, burstlines, burstbytes/1024./5.0/1024.)
                burstlines = 0
                burstbytes = 0

            if current<start or current>stop:
                continue

            if diff>1:
                print "skip .. %02d:%02d:%02d, start: %02d:%02d, diff: %d" \
                    % (hour, minute, second, lastminute, lastsecond, diff)
            
if __name__ == "__main__":
    main()




    
