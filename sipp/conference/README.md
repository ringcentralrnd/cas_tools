
# SIPP RC Conference load tool

## We are using modified sipp tools
Compiled versions are https://bitbucket.org/ringcentralrnd/sipp/downloads

1. sipp compiled with pcap support
2. sipp has modified dashboard output

Prefer version is linux sipp.CentOS.release.6.6 Download it and rename. It requres sudo rights for access to raw sockets.

## RCC requres phone number and pin to entire to the conference.
List of phones for environments  
https://wiki.ringcentral.com/display/ENG/RCC+for+LAB  
PINS should be generated.

* RND-RCC-AMS pins 20.pins.ams1 100.pins.ams1
* RND-MSG-SPB part 2 pins 100.pins.rnd_msg_spb2 20.pins.rnd_msg_spb2 40.pins.rnd_msg_spb2
  
## sipp scenario requres csv file
This is scenario parametrs.
It can be generated with generate_csv.py from pin file.
Example ./generate_csv.py -f 5x100.csv --delay 1 1 --phone 16502410588 -n 5 --cps 5 --pinsfile 100.pins.rnd_msg_spb2

## Scenarios:
- **load.xml** - continuous scenario. Generate continuous load on server. Allow easy to hear what happens in this conference. Has synchronized users conference leaving.
- **performance.xml** - for performance testing. All users entire and leave the conference evenly. It allows to find max server performance.
- **recovery.xml** - recovery scenario. All users entire to the conference and dont leave it so longer. 120 sec. It allows to crash server and check all incoming invites.

## Example of usage - recovery test on RND-RCC-AMS environment
sipp 192.168.33.162:5060 -sf ./recovery.xml -i 192.168.42.22 -skip_rlimit -r 24 -m 180 -l 180 -inf ./3x20x3_ams01.csv -trace_logs -trace_err -trace_msg

- 192.168.42.22 - my local ip
- 192.168.33.162:5060 - SONUS address
- -sf ./recovery.xml - recovery scenario
- -r 24 - calls per second
- -m 180 -l 180 - calls limit
- -inf ./3x20x3_ams01.csv - conference users

# check rnd01-t05-rcs01, rnd01-t05-rcs02 servers
- ssh root@rnd01-t05-rcs01
- tail -f /var/log/ringcentral/rcs/rcs-smc_svc.log









