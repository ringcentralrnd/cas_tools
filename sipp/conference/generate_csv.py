#!/usr/bin/python

import argparse, csv
import os.path
import math


PHONE_DEFAULT = '16502410550'
PIN_DEFAULT = '729792'
DELAY_DEFAULT = 10.0
ENTRIES_N = 10
csv_template = '''SEQUENTIAL
'''
names = sorted(['Sarah', 'Bob', 'Alice', 'Peter', 'John', 'Bill', 'Mallory'])


def check_negative(value):
    try:
        ivalue = int(value)
        assert ivalue > 0
    except:
        raise argparse.ArgumentTypeError('Expected a positive integer, found {0}'.format(value))
    else:
        return ivalue


def check_delay(value):
    try:
        fvalue = float(value)
        assert fvalue >= 0
    except:
        raise argparse.ArgumentTypeError('Expected a positive number, found {0}'.format(value))
    else:
        return fvalue


def check_pin(value):
    try:
        pin = str(value)
        assert len(pin) == 6 and pin.isdigit()
    except:
        raise argparse.ArgumentTypeError('Expected 6 digits, found {0}'.format(pin))
    else:
        for i in xrange(len(pin) - 1):
            if pin[i] == pin[i + 1]:
                raise argparse.ArgumentTypeError(
                    'PIN cannot contain two identical digits in a row (found {0}{1} in PIN {2}).'.format(pin[i], pin[i + 1], pin))

        return pin


def check_pins_file(filename):
    if not os.path.isfile(filename):
        raise argparse.ArgumentTypeError('File [{0}] does not exist'.format(filename))

    with open(filename, 'r') as pinsfile:
        pins = []
        
        for line in pinsfile:
            pin = line.replace('\n', '').replace('\r', '')
            if pin:
                if not len(pin) == 6:
                    raise argparse.ArgumentTypeError(
                        'Expected 6 digits, found: {0}'.format(len(pin)))
                if not pin.isdigit():
                    raise argparse.ArgumentTypeError(
                        'Expected digits only, found: {0}'.format(pin))
                    
                for i in xrange(len(pin) - 1):
                    if pin[i] == pin[i + 1]:
                        raise argparse.ArgumentTypeError(
                            'PIN cannot contain two identical digits in a row (found {0}{1} in PIN {2}).'.format(pin[i], pin[i + 1], pin))
                pins.append(pin)

        return pins

    
def check_filename(value):
    filename = str(value)
    if not filename.endswith('.csv'):
        filename += '.csv'
    return filename
    

parser = argparse.ArgumentParser(description='Generate csv files for SIPp. CSV fields: [name, phone, delay, pin]. Delays are generated independently for each pin.')

parser.add_argument('-f', action='store', metavar='filename', type=check_filename, dest='filename', required=True, help='CSV file name to write')

parser.add_argument('--delay', action='store', metavar='delay', type=check_delay, dest='delay', help='Delay in seconds: [base delay] (default: {0})'.format(DELAY_DEFAULT))

parser.add_argument('--phone', action='store', metavar='phone_number', type=str, dest='phone', help='Phone number')

parser.add_argument('-n', action='store', metavar='n_participants', type=check_negative, dest='n_participants', required=True, help='Number of participants')

parser.add_argument('-r', action='store', metavar='repeats', type=check_negative, dest='repeats', required=False, help='Repeat scenario N times')

parser.add_argument('--cps', action='store', metavar='CPS', type=check_negative, dest='cps', required=True, help='Calls per second')

parser.add_argument('-v', action='store_const', metavar='verbose', dest='verbose', required=False, const=True, help='Be more verbose')

pins_group = parser.add_mutually_exclusive_group(required=True)
pins_group.add_argument('--pins', nargs='+', action='store', metavar='pin', type=check_pin, dest='pins', help='Provide pins list separated by a space or a path to a file contaiting one pin on each line')
pins_group.add_argument('--pinsfile', action='store', metavar='pin file', type=check_pins_file, dest='pins', help='Provide pins list separated by a space or a path to a file contaiting one pin on each line')


parser.set_defaults(phone=PHONE_DEFAULT, pins=[PIN_DEFAULT], entries_n=ENTRIES_N, delay=DELAY_DEFAULT, repeats=1)


def write_csv(filename, phone, base_delay, pins, n_participants, cps, repeats):
    with open(filename, 'w') as csvfile:
        csvfile.write(csv_template)
        writer = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
        n_conferences = len(pins)

        for i in xrange(repeats):
            scenario_len = n_participants * n_conferences
            for j in xrange(scenario_len):
                idx = i*scenario_len + j
                name = '{0}{1}'.format(names[j % len(names)], idx)
            
                participant_j = j / n_conferences
                delay_before = int(base_delay * 1000)
                delay_after = int((n_participants - participant_j - 1) * n_conferences * 1000 / float(cps))
                
                row = [name, phone, delay_before]
                row += list(str(pins[j % len(pins)]))
                row += [delay_after]
                writer.writerow(row)

    
if __name__ == '__main__':
    args = parser.parse_args()
    write_file = False

    if os.path.isfile(args.filename):
        print 'File [{0}] already exists. Overwrite? (y/N) '.format(args.filename),
        answer = raw_input()
        if answer.startswith('y') or answer.startswith('Y'):
            write_file = True
    else:
        write_file = True
    
    if write_file:
        write_csv(args.filename, args.phone, args.delay, args.pins, args.n_participants, args.cps, args.repeats)
        print 'Wrote {0}'.format(args.filename)

        if args.verbose:
            with open(args.filename) as f:
                print f.read()

