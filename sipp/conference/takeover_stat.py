#!/usr/bin/python

import pyparsing as pyp
import itertools
import math

integer = pyp.Word(pyp.nums)

# day hour min sec

class Parser(object):
    def __init__(self):
        date = integer + "-" + integer + "-" + integer + "T" + integer + ":" + integer + ":" + integer + "." + integer
        self.__pattern = date + pyp.Regex(".*")

    def parse(self, line):
        parsed = self.__pattern.parseString(line)

        return parsed

parser = Parser()
    
def _time(line):
    try:
        fields = parser.parse(line)
    except pyp.ParseException:
        retrun (False,)

    day = int(fields[4])
    hour = int(fields[6])
    minute = int(fields[8])
    second = int(fields[10])
    micsec = int(fields[12])
    
    return (True, (day, hour, minute, second, micsec))
                                    

def print_metric(line, name, (day, hour, minute, second, micsec)):
    pos = line.find(name)
    if pos>0:
        print "%02d:%02d:%02d %s" % (hour, minute, second, line[pos:len(line)-1])

def main():

    lastsec = 0
    rate = 0
    total=0

    with open('./rcs-smc_svc.log') as syslogFile:
        for line in syslogFile:
        
            if not (("restore_call_req" in line) \
                    or ("|  callCount" in line) \
                    or ("callEstablishedCount" in line)): continue

            (Success, Time) = _time(line)

            if Success == False:
                continue

            (day, hour, minute, second, micsec) = Time

            if "restore_call_req" in line:
                if lastsec != second:
                    print "%02d:%02d:%02d rate:%d"  % (hour, minute, lastsec, rate)
                    lastsec = second
                    total = total + rate
                    rate = 0
                rate = rate + 1

            print_metric(line, "|  callEstablishedCount", Time)
            print_metric(line, "|  callCount", Time)
            

    print "total: %d" % (total)
            
if __name__ == "__main__":
    main()




    
